License Pending choice (will be cc or cern ohl probably)

Designed by Chipperdoodles (aka BulBul)
2024

The BulBul Writer M0118 is a replacement circuit board meant to fit in the case of an apple m0116/8 keyboard. It is designed to reuse all the parts so it should be a drop in and inteded to reuse the plate and keycaps and switches (depending on your switch condition and preference). I haven't ordered/tested pcbs (hence the should part) yet but I'm uploading the files just to get them up somewhere. It supports both the ANSI and ISO layouts of the apple standard keyboard and only alps switches. 

The controller is an NRF52840 based module and once I get a pcb built I will build firmware for it using ZMK. The battery is intended to be a AAA sized LiFePo4. These are uncommon but do exist, if building your own and you want to use a normal lithium ion cell you can, but you will have to change the configuration resistors for the BQ25185 charging IC. I also intend to design some 3d printed hardware to help match the tall USB-C receptacles with the outline of the old mini-din connectos to help them fit snugly in the case. (they're held in place by some pressure from a leg on the upper case). I also intend to design a 3d printed keycap for the power button that will house a MIP or OLED display. Another thing of note is that the power button is meant to be a locking alps switch (reused from the old m0116/8) to act as Enable/disable switch for the power regulator. Modern BLE is usually meant to be always on but I find comfort in having a true off/on switch.

Changes:
3/28/2024 - Upload